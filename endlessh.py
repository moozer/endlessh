#!/usr/bin/env python3

import asyncio
import random
import sys

prog_indicator = ['-', '/', '|', '\\']

port = 2222

async def handler(_reader, writer):
    counter = 0
    sys.stdout.write( "New connection...\n" )
    try:
        while True:
            await asyncio.sleep(1)
            writer.write(b'%x\r\n' % random.randint(0, 2**32))
            sys.stdout.write( prog_indicator[counter%len(prog_indicator)] )
            sys.stdout.flush()
            await writer.drain()
            counter = counter + 1
    except ConnectionResetError:
        sys.stdout.write("Connection closed\n")
        pass

async def main():
    server = await asyncio.start_server(handler, '0.0.0.0', port)
    async with server:
        await server.serve_forever()

print( "Waiting for ssh connection on port {}".format(port) )
print( "use CTRL+C to shut down")

try:
    asyncio.run(main())
except KeyboardInterrupt:
    print( "CTRL+C caught. closing" )
